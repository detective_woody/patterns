interface Component {
    accept(visitor: Visitor): number;
}

interface Visitor {
    visitCompany(company: Company): number;
    visitDepartment(department: Department): number;
}

class Employee {
    position: string;
    salary: number;

    constructor(position: string, salary: number) {
        this.position = position;
        this.salary = salary;
    }
}

class Department implements Component {
    public name: string;
    public employees: Employee[];
    
    constructor(name: string, employees: Employee[] = []) {
        this.name = name;
        this.employees = employees
    }

    addEmployee(employee: Employee): void {
        this.employees.push(employee);
    }

    accept(visitor: Visitor): number {
        return visitor.visitDepartment(this);
    }
}

class Company implements Component {
    public name: string;
    public departments: Department[];

    constructor(name: string, departments: Department[] = []) {
        this.name = name;
        this.departments = departments;
    }

    addDepartment(department: Department): void {
        this.departments.push(department);
    }

    accept(visitor: Visitor): number {
        return visitor.visitCompany(this);
    }
}

class SalaryCalculator implements Visitor {
    visitCompany(company: Company): number {
        let totalSalary = 0;
        for (const department of company.departments) {
        totalSalary += department.accept(this);
        }
        return totalSalary;
    }

    visitDepartment(department: Department): number {
        let totalSalary = 0;
        for (const employee of department.employees) {
        totalSalary += employee.salary;
        }
        return totalSalary;
    }
}

const engineering = new Department("Engineering");
engineering.addEmployee(new Employee("Engineer", 60000));
engineering.addEmployee(new Employee("Manager",80000));

const sales = new Department("Sales");
sales.addEmployee(new Employee("Salesperson", 50000));

const myCompany = new Company("My Company", [engineering, sales]);

const salaryCalculator = new SalaryCalculator();

const companySalary = myCompany.accept(salaryCalculator);
console.log(`Total salary for ${myCompany.name}: ${companySalary}`);

for (const department of myCompany.departments) {
  const departmentSalary = department.accept(salaryCalculator);
  console.log(`Total salary for ${department.name}: ${departmentSalary}`);
}