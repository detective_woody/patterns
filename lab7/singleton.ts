class Singleton {
    private static instance: Singleton;

    protected constructor() { }

    public static getInstance(): Singleton {
        if (!this.instance) {
            this.instance = new this();
        }

        return this.instance;
    }

    public connect() {}
}

class MongoDB_DB extends Singleton {

    public connect() {
        console.log("Connectiong to MongoDB")
    }
    
}

class PostgreSQL_DB extends Singleton {

    public connect() {
        console.log("Connectiong to PostgreSQL")
    }

}

// const db1 = PostgreSQL_DB.getInstance();
// const db2 = MongoDB_DB.getInstance();

// db1.connect()
// db2.connect()

// const db3 = MongoDB_DB.getInstance();
// db3.connect();

// console.log(db2 === db3)