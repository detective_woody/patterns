interface QueryBuilder {
  select(columns: string[], table: string): QueryBuilder;
  where(condition: string): QueryBuilder;
  limit(limit: number): QueryBuilder;
  getSQL(): string;
}

class MySQLQueryBuilder implements QueryBuilder {
  private query: string = '';

  select(columns: string[], table: string): QueryBuilder {
    this.query += `SELECT ${columns.join(', ')} FROM ${table} `;
    return this;
  }

  where(condition: string): QueryBuilder {
    this.query += `WHERE ${condition} `;
    return this;
  }

  limit(limit: number): QueryBuilder {
    this.query += `LIMIT ${limit} `;
    return this;
  }

  getSQL(): string {
    return this.query;
  }
}

class PostgreSQLQueryBuilder implements QueryBuilder {
  private query: string = '';

  select(columns: string[], table: string): QueryBuilder {
    this.query += `SELECT ${columns.join(', ')} FROM ${table} `;
    return this;
  }

  where(condition: string): QueryBuilder {
    this.query += `WHERE ${condition} `;
    return this;
  }

  limit(limit: number): QueryBuilder {
    this.query += `LIMIT ${limit} `;
    return this;
  }

  getSQL(): string {
    return this.query;
  }
}


// Usage example
const MySQLBuilder = new MySQLQueryBuilder();
const query1 = MySQLBuilder
      .select(["name", "email"], "users")
      .where("age > 18")
      .limit(10)
      .getSQL();
console.log(query1); // Output: SELECT name, email FROM users WHERE age > 18 LIMIT 10

const PostgreSQLBuilder = new PostgreSQLQueryBuilder();
const query2 = PostgreSQLBuilder
      .select(["name", "email"], "users")
      .where("age < 18")
      .limit(5)
      .getSQL();
console.log(query2); // Output: SELECT name, email FROM users WHERE age < 18 LIMIT 5 