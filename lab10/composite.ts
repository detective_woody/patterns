abstract class FormElement {
    protected readonly children: FormElement[] = [];
    
    public add(element: FormElement) {
      this.children.push(element);
    }
        
    public abstract render(): string;
}
  
class InputField extends FormElement {
    private readonly name: string;
    private readonly type: string;

    constructor(name: string, type: string) {
        super();
        this.name = name;
        this.type = type;
    }
    
    public render(): string {
      return `<input type="${this.type}" name="${this.name}" />`;
    }
}
  
class SelectField extends FormElement {
    private readonly name: string;
    private readonly options: string[];
    
    constructor(name: string, options: string[]) {
        super();
        this.name = name;
        this.options = options;
    }
    
    public render(): string {
      const optionTags = this.options.map(option => `<option>${option}</option>`).join('');
      return `<select name="${this.name}">${optionTags}</select>`;
    }
}
  
class Fieldset extends FormElement {
    constructor(private readonly legend: string) {
      super();
    }
    
    public render(): string {
      const childElements = this.children.map(child => child.render()).join('');
      return `<fieldset><legend>${this.legend}</legend>${childElements}</fieldset>`;
    }
}

// Example usage
const form = new Fieldset('Personal Information');
form.add(new InputField('name', 'text'));
form.add(new InputField('email', 'email'));

const addressFieldset = new Fieldset('Address');
addressFieldset.add(new InputField('street', 'text'));
addressFieldset.add(new InputField('city', 'text'));
addressFieldset.add(new SelectField('state', ['CA', 'NY', 'TX']));
form.add(addressFieldset);

console.log(form.render());

