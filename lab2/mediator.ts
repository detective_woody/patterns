interface Mediator {
    notify(sender: object, event: string): void;
}

class ConcreteMediator implements Mediator {
    private dateSelector: DateComponent;
    private timeSelector: TimeComponent;
    private recipientCheckbox: RecipientCheckboxComponent;
    private nameInput: NameInputComponent;
    private phoneInput: PhoneInputComponent;
    private pickupCheckbox: PickupCheckboxComponent;

    constructor(
        dateSelector: DateComponent,
        timeSelector: TimeComponent,
        recipientCheckbox: RecipientCheckboxComponent,
        nameInput: NameInputComponent,
        phoneInput: PhoneInputComponent,
        pickupCheckbox: PickupCheckboxComponent
        ) 
    {
        this.dateSelector = dateSelector;
        this.dateSelector.setMediator(this);

        this.timeSelector = timeSelector;
        this.timeSelector.setMediator(this);

        this.recipientCheckbox = recipientCheckbox;
        this.recipientCheckbox.setMediator(this);

        this.nameInput = nameInput;
        this.nameInput.setMediator(this);

        this.phoneInput = phoneInput;
        this.phoneInput.setMediator(this);

        this.pickupCheckbox = pickupCheckbox;
        this.pickupCheckbox.setMediator(this);
    }

    
    notify(sender: object, event: string): void {
        if (sender === this.dateSelector && event === 'dateSelected') {
            const availableTime = this.getAvailableTime(this.dateSelector.selectedDate);
            this.timeSelector.updateAvailableTime(availableTime);
        } else if (sender === this.recipientCheckbox && event === 'recipientCheckboxChanged') {
            if (this.recipientCheckbox.isChecked()) {
                this.nameInput.show();
                this.phoneInput.show();
            } else {
                this.nameInput.hide();
                this.phoneInput.hide();
            }
        } else if (sender === this.pickupCheckbox && event === 'pickupCheckboxChanged') {
            if (this.pickupCheckbox.isChecked()) {
                this.dateSelector.hide();
                this.timeSelector.hide();
                this.recipientCheckbox.hide();
                this.nameInput.hide();
                this.phoneInput.hide();
            } else {
                this.dateSelector.show();
                this.timeSelector.show();
                this.recipientCheckbox.show();
                this.nameInput.show();
                this.phoneInput.show();
            }
        }
    }

    // Helper method to get the available time slots for a given day
    private getAvailableTime(date: Date): string[] {
            if (date.getDay() === 5 || date.getDay() === 6) {
                return ["8am to 8pm"];
            } else {
                return ["10am to 12am", "1pm to 5pm"];
            
        }
    }
}

interface Component {
    setMediator(mediator: Mediator): void;
    show(): void;
    hide(): void;
}
  

class DateComponent implements Component {
    private mediator: Mediator;
    public selectedDate: Date;

    setMediator(mediator: Mediator): void {
        this.mediator = mediator;
    }

    show(): void {
        // Show implementation
    }

    hide(): void {
        // Hide implementation
    }

    
    private onDateSelected(): void {
        this.selectedDate = new Date;// Get date from the form
        this.mediator.notify(this, 'dateSelected');
    }
}

class TimeComponent implements Component {
    private mediator: Mediator;
    private availableTime: string[];

    setMediator(mediator: Mediator): void {
        this.mediator = mediator;
    }

    show(): void {
    }

    hide(): void {
    }

    updateAvailableTime(availableTime: string[]): void {
        this.availableTime = availableTime;
        // Implementation of displaying of available time
    }

    private onTimeSelected(): void {
    }
}

class RecipientCheckboxComponent implements Component {
    private mediator: Mediator;
    private checkbox: HTMLInputElement;

    setMediator(mediator: Mediator): void {
        this.mediator = mediator;
    }

    show(): void {
    }

    hide(): void {
    }

    isChecked(): boolean {
        return this.checkbox.checked;
    }

    private onCheckboxChanged(): void {
        this.mediator.notify(this, 'recipientCheckboxChanged');
    }
}

class NameInputComponent implements Component {
    private mediator: Mediator;
    private input: HTMLInputElement;

    setMediator(mediator: Mediator): void {
        this.mediator = mediator;
    }

    show(): void {
    }

    hide(): void {
    }
}

class PhoneInputComponent implements Component {
    private mediator: Mediator;
    private input: HTMLInputElement;

    setMediator(mediator: Mediator): void {
        this.mediator = mediator;
    }

    show(): void {
    }

    hide(): void {
    }
}

class PickupCheckboxComponent implements Component {
    private mediator: Mediator;
    private checkboxChecked: boolean;

    setMediator(mediator: Mediator): void {
        this.mediator = mediator;
    }

    show(): void {
    }

    hide(): void {
    }

    isChecked(): boolean {
        return this.checkboxChecked;
    }

    
    private onCheckboxChanged(): void {
        this.mediator.notify(this, 'pickupCheckboxChanged');
    }
}

const dateComp = new DateComponent();
const timeComp = new TimeComponent();
const recipientComp = new RecipientCheckboxComponent();
const nameComp = new NameInputComponent();
const phoneComp = new PhoneInputComponent();
const selfPickupComp = new PickupCheckboxComponent();

const mediator = new ConcreteMediator(dateComp, timeComp, recipientComp, nameComp, phoneComp, selfPickupComp)

