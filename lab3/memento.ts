class PreferencesState {
    preference_update_subscribed: boolean;
    preference_privacy_policy_subscribed: boolean;
    preference_sale_subscribed: boolean;

    constructor(update: boolean, privacy_policy: boolean, sale:boolean) {

        this.preference_update_subscribed = update;
        this.preference_privacy_policy_subscribed = privacy_policy;
        this.preference_sale_subscribed = sale;
    }
}

class Memento {

    private state: PreferencesState;

    constructor(update: boolean = false, privacy_policy: boolean = false, sale:boolean = false) {
        this.state = new PreferencesState(update, privacy_policy, sale)
    }

    public getState(): PreferencesState {
        return this.state;
    }
}

class UserAccount{
    // User account implementation

    // Preferences for mailing list subscriptions
    private preference_update_subscribed: boolean;
    private preference_privacy_policy_subscribed: boolean;
    private preference_sale_subscribed: boolean;

    constructor(update: boolean = false, privacy_policy: boolean = false, sale:boolean = false) {

        this.preference_update_subscribed = update;
        this.preference_privacy_policy_subscribed = privacy_policy;
        this.preference_sale_subscribed = sale;

    }

    public changeUpdateMailingPreference(): void {
        this.preference_update_subscribed = !this.preference_update_subscribed
    }

    public changePrivacyPolicyMailingPreference(): void {
        this.preference_privacy_policy_subscribed = !this.preference_privacy_policy_subscribed
    }

    public changeSaleMailingPreference(): void {
        this.preference_sale_subscribed = !this.preference_sale_subscribed
    }

    
    public debugShowPreferences(): void {
        let res = ""
        res += this.preference_update_subscribed ? "1 " : "0 "
        res += this.preference_privacy_policy_subscribed ? "1 " : "0 "
        res += this.preference_sale_subscribed ? "1" : "0"
        console.log(res)

    }

    public saveState(): Memento{
        return new Memento(this.preference_update_subscribed, this.preference_privacy_policy_subscribed, this.preference_sale_subscribed);
    }

    public restore(memento: Memento) {
        let state = memento.getState();
        this.preference_privacy_policy_subscribed = state.preference_privacy_policy_subscribed;
        this.preference_sale_subscribed = state.preference_sale_subscribed;
        this.preference_update_subscribed = state.preference_update_subscribed;
    }
}

class Caretaker {
    private previousMemento: Memento;

    private originator: UserAccount;

    constructor(originator: UserAccount) {
        this.originator = originator;
    }

    public savePreviousPreferences(): void {
        this.previousMemento = this.originator.saveState();
    }

    public undo(): void {
        this.originator.restore(this.previousMemento);
    }
}

const obj = new UserAccount();
const caretaker = new Caretaker(obj);

console.log("Show original preferences");
obj.debugShowPreferences();
caretaker.savePreviousPreferences();

console.log("Change and show new preferences");
obj.changeUpdateMailingPreference();
obj.changePrivacyPolicyMailingPreference();
obj.debugShowPreferences();

console.log("Undo and show reverted preferences");
caretaker.undo();
obj.debugShowPreferences();

