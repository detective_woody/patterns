class foodDeliveryApp {
    private strategy: Strategy;

    constructor(strategy: Strategy) {
        this.strategy = strategy;
    }

    public setStrategy(strategy: Strategy) {
        this.strategy = strategy;
    }

    public calculatePrice(): number {
        const foodPrice = 50;
        const deliveryFee = this.strategy.calculate();
        const result = foodPrice + deliveryFee;

        return result;
    }


}


interface Strategy {
    calculate(): number;
}

class selfPickUp implements Strategy {
    public calculate(): number {
        return 0;

    }
}

class thirdPartyDeliveryService implements Strategy {
    public calculate(): number {
        return 100;

    }
}

class ownDeliveryService implements Strategy {
    public calculate(): number {
        return 50;

    }
}


const app = new foodDeliveryApp(new selfPickUp());
console.log(app.calculatePrice());

app.setStrategy(new thirdPartyDeliveryService());
console.log(app.calculatePrice());
