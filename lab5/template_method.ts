abstract class BaseClass {
    public update(): void {
        this.getObject()
    }

    protected getObject(): object {
        return {};
    }

    protected validateOutput(): void {
        console.log("Validation")
    }

    protected saveData(): void {

    }

    protected respond(): object {
        const code = 200;
        const responseStatus = "OK";
        return {responseCode: code, responseStatus: responseStatus};
    }

    protected hook(): void {}
}

class Product extends BaseClass {

    protected validateOutput(): void {
        this.notifyAdmin();
    }

    private notifyAdmin(): void {
        console.log("NOTIFICATION TO ADMIN: Product failed validation");
    }
}

class User extends BaseClass {

    private readonly email: string; 

    constructor(email: string) {
        super();
        this.email = email;
    }

    // protected saveData(): void {
    //     // const 
    //     let newEmail = "123"

    //     newEmail = this.email;
    //     this.save();
    // }

    // private save(): void {
    //     console.log("LOGGER: Data saved")
    // }


}

class Order extends BaseClass {


    protected respond(): object {
        const responseCode = 200;
        const responseMessage = "OK";
        const product = {name:"Product 1"}
        return {responseCode, responseMessage, product}
    }
}
