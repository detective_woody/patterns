class TourPlace {
    //Tour place class implementation
}

abstract class BaseIterator {
    protected collection: PlacesCollection;

    protected index: number = 0;

    abstract next(): TourPlace;

    constructor(collection: PlacesCollection) {
        this.collection = collection;
    }

    public reset(): void {
        this.index = 0;
    }

    public valid(): boolean {
        const isValid = this.index < this.collection.getItems().length;
        if (!isValid) { this.index = 0}
        return isValid;
    }
}

class TouristPreferenceIterator extends BaseIterator {

    constructor(collection: PlacesCollection) {
        super(collection);
    }

    public next(): TourPlace {
        // Implementation of iterator for tourist's own preference

        return TourPlace;
    }

}
class MapSuggestionIterator extends BaseIterator {

    constructor(collection: PlacesCollection) {
        super(collection);
    }

    public next(): TourPlace {
        // Implementation of iterator for map suggested places to visit

        return TourPlace;
    }

}
class TourGuideSuggestionIterator extends BaseIterator {

    constructor(collection: PlacesCollection) {
        super(collection);
    }

    public next(): TourPlace {
        // Implementation of iterator for tour guide suggested places to visit
    
        return TourPlace;
    }

}

class PlacesCollection {
    private items: TourPlace[] = [];

    public getItems(): TourPlace[] {
        return this.items;
    }

    public addItem(item: TourPlace):void {
        this.items.push(item);
    }

    public getTouristPreferenceIterator(): BaseIterator {
        return new TouristPreferenceIterator(this);
    }

    public getMapSuggestionIterator(): BaseIterator {
        return new MapSuggestionIterator(this);
    }

    public getTourGuideSuggestionIterator(): BaseIterator {
        return new TourGuideSuggestionIterator(this);
    }

}
